# docker-ssh

A tool to connect to the docker daemon through ssh.

## Command line

### Installation
```
npm install -g docker-ssh
```

### Usage
```
docker-ssh --ssh-host <host> <docker_command>...
```

### Example
```
docker-ssh --ssh-host user@example.com ps -aq
```

## Javascript

The javascript version of this is a thin wrapper around [dockerode](https://www.npmjs.com/package/dockerode)

### Installation
```
npm install docker-ssh
```

### Usage

`docker.ssh` returns an instance of [dockerode](https://www.npmjs.com/package/dockerode).
This module only handles the setup/teardown of the ssh tunnel

```javascript
const docker = require('docker-ssh')
async function main() {
  const {client, cleanup} = await docker.ssh('user@example.com')
  let containers = await client.listContainers()
  // code
}
main()
```
