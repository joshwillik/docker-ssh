const child_process = require('child_process')
const fs = require('fs')
const Docker = require('dockerode')
const task = require('@joshwillik/task')
let E = module.exports

E.open_socket = host => task(async function ssh_docker_socket() {
  let socket_path = `/tmp/${host}_docker.sock`
  let cmd = [
    'ssh',
    '-nNT',
    '-L',
    `${socket_path}:/var/run/docker.sock`,
    host,
  ]
  let ssh_error
  let ssh_process = child_process.exec(cmd.join(' '), function on_ssh_exit(err) {
    if (err) {
      return ssh_error = err
    }
    if (file_exists(socket_path)) {
      fs.unlinkSync(socket_path)
    }
  })
  ssh_process.stderr.pipe(process.stderr)
  while (!file_exists(socket_path)) {
    await task.sleep(1e3)
    if (ssh_error) {
      throw ssh_error
    }
  }
  return {
    socket_path,
    cleanup() { ssh_process.kill() },
  }
})

E.ssh = async function ssh_client(host) {
  let {socket_path, cleanup} = await E.open_socket(host)
  let client = new Docker({socketPath: socket_path})
  return {client, cleanup}
}

function file_exists(path) {
  try {
    fs.statSync(path)
    return true
  } catch(e) {
    return false
  }
}
