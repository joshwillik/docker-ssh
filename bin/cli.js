#!/usr/bin/env node
const child_process = require('child_process')
const {docopt} = require('docopt')
const task = require('@joshwillik/task')
const docker = require('..')

task(async function() {
  this.catch(err => {
    console.error(err)
    process.exit(err.code || 1)
  })
  let options = docopt(`Usage: docker-ssh <host> [-- <docker-commands>...]`)
  let {socket_path, cleanup} = await docker.open_socket(options['<host>'])
  this.finally(cleanup)
  let docker_child = child_process.spawn(`docker`, options['<docker-commands>'], {
    env: {
      DOCKER_HOST: `unix://${socket_path}`,
    },
    stdio: 'inherit',
  })
  docker_child.on('close', code => this.continue(code))
  return await this.wait()
})
