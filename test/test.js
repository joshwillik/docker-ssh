#!/usr/bin/env node
const docker = require('..')
const {docopt} = require('docopt')

async function main() {
  let options = docopt(`Usage: test.js <host>`)
  let {client, cleanup} = await docker.ssh(options['<host>'])
  let containers = await client.listContainers()
  console.log(containers)
  cleanup()
}
if (!module.parent) {
  main().catch(e => console.error(e))
}
